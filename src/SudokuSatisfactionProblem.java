// Bobby Youstra
// Sudoku

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class SudokuSatisfactionProblem extends ConstraintSatisfactionProblem {
    private int size;
    private int block_size;

    private Variable[][] board;

    public class Variable extends ConstraintSatisfactionProblem.Variable<Integer> {
        private boolean assigned = false;
        private int row;
        private int col;
        private ArrayList<Constraint> constraints;

        private HashSet<Integer> domain;
        private HashSet<Integer> oldDomain;

        public Variable(int row, int col) {
            this.row = row;
            this.col = col;

            constraints = new ArrayList<>((int)Math.pow(size, 2));
            domain = new HashSet<>(size);

            for(int i = 1; size + 1 > i; ++i) {
                domain.add(i);
            }
        }

        public boolean assigned() {
            return assigned;
        }

        @Override
        public String name() {
            return "(" + row + "," + col + ")";
        }

        public void addConstraint(Constraint constraint) {
            // TODO(Bobby): Domain restriction from constraint code
            oldDomain = (HashSet<Integer>)domain.clone();

            constraints.add(constraint);

            updateDomainForConstraint(constraint);
        }

        private void updateDomainForConstraint(Constraint constraint) {
            if(constraint instanceof SudokuEqualConstraint) {
                for(int i = 1; size + 1 > i; ++i) {
                    if(!((SudokuEqualConstraint) constraint).satisfiedBy(i)) {
                        domain.remove(i);
                    }
                }
                assigned = true;
            } else if(constraint instanceof SudokuNotEqualConstraint) {
                if(((SudokuNotEqualConstraint) constraint).left != this &&
                        ((SudokuNotEqualConstraint) constraint).right == this) {
                    if(((SudokuNotEqualConstraint) constraint).left.assigned() &&
                        ((SudokuNotEqualConstraint) constraint).left.domain().size() > 0) {
                        domain.remove(((SudokuNotEqualConstraint) constraint).
                                left.domain().iterator().next());
                    }
                } else if(((SudokuNotEqualConstraint) constraint).right != this &&
                        ((SudokuNotEqualConstraint) constraint).left == this) {
                    if(((SudokuNotEqualConstraint) constraint).right.assigned() &&
                        ((SudokuNotEqualConstraint) constraint).right.domain().size() > 0) {
                        domain.remove(((SudokuNotEqualConstraint) constraint).
                                right.domain().iterator().next());
                    }
                }
            }
        }

        public void removeLastConstraint() {
            // Removes the last added constraint.
            // This function is really useful when solving
            // with a backtracking algorithm because
            // it allows easy reversal of the board
            // into its original state.

            domain = oldDomain;

            if(constraints.get(constraints.size() - 1)
                    instanceof SudokuEqualConstraint) {
                assigned = false;
            }

            constraints.remove(constraints.size() - 1);
        }

        @Override
        public void addToDomain(Integer value) {
            domain.add(value);
        }

        @Override
        public void removeFromDomain(Integer value) {
            domain.remove(value);
        }

        @Override
        public boolean isInDomain(Integer value) {
            return domain.contains(value);
        }

        @Override
        public HashSet<Integer> domain() {
            return domain;
        }

        @Override
        public Iterator<Constraint> constraints() {
            return constraints.iterator();
        }
    }

    class SudokuEqualConstraint extends ConstraintSatisfactionProblem.EqualConstraint<Integer> {
        public SudokuEqualConstraint(Variable variable, Integer value) {
            super(variable, value);
        }

        @Override
        public String toString() {
            return variable().name() + " = " + value;
        }
    }

    class SudokuNotEqualConstraint extends NotEqualConstraint<Integer> {
        public SudokuNotEqualConstraint(Variable left, Variable right) {
            super(left, right);
        }

        @Override
        public boolean satisfiedBy(Integer left, Integer right) {
            if(left == 0 && right == 0) {
                return true;
            }

            return super.satisfiedBy(left, right);
        }

        @Override
        public String toString() {
            return left.name() + " != " + right.name();
        }
    }

    /*public SudokuSatisfactionProblem(int[][] initialBoard) {
        assert initialBoard.length == initialBoard[0].length;

        this.initialBoard = initialBoard;

        size = initialBoard.length;

        block_size = (int)Math.sqrt(size);
        board = new Variable[size][size];
    }*/

    private int sqrt(int n) {
        // Adapted from Original Homework #5 Sudoku Board
        // Returns ceiling(sqrt(n))
        int result = 1;
        while (result * result  < n) {
            result++;
        }
        return result;
    }

    public SudokuSatisfactionProblem(String s) {
        // Adapted from Original Homework #5 Sudoku Board

        String[] values = s.split("[ \\-\\+|\\n]+");
        this.size = sqrt(values.length);
        this.block_size = sqrt(size);
        if (block_size * block_size != size) {
            throw new IllegalArgumentException();
        }

        this.board = new Variable[size][size];

        int k = 0;

        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                String val = values[k++].trim();
                Variable var = new Variable(r, c);

                if (!val.equals(".")) {
                    int value = Integer.parseInt(val);

                    if (value <= 0 | value > size) {
                        throw new IllegalArgumentException();
                    }

                    var.addConstraint(new SudokuEqualConstraint(var, value));
                }

                this.board[r][c] = var;
            }
        }
    }

    /*public SudokuSatisfactionProblem(String s, int blank) {
        String[] values = s.split(".");
        this.size = (int)Math.sqrt(values.length);
        this.block_size = (int)Math.sqrt(size);
        if (block_size * block_size != size) {
            throw new IllegalArgumentException();
        }

        this.board = new Variable[size][size];

        int k = 0;

        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                Integer val = Integer.parseInt(values[k++]);
                Variable var = new Variable(r, c);

                if (val != blank) {

                    if (val <= 0 | val > size) {
                        throw new IllegalArgumentException();
                    }

                    var.addConstraint(new SudokuEqualConstraint(var, val));
                }

                this.board[r][c] = var;
            }
        }
    }*/

    private void generateConstraints(int row, int col) {
        // Constraint generator for sudoku columns
        for(int r = 0; size > r; ++r) {
            if(r != row) {
                board[row][col].addConstraint(new SudokuNotEqualConstraint(
                                                                board[row][col],
                                                                board[r][col]));
            }
        }

        // Constraint generator for sudoku rows
        for(int c = 0; size > c; ++c) {
            if(c != col) {
                board[row][col].addConstraint(new SudokuNotEqualConstraint(
                                                                board[row][col],
                                                                board[row][c]));
            }
        }

        // Constraint generator for sudoku blocks
        int blockRow = (row / block_size) * block_size;
        int blockCol = (col / block_size) * block_size;

        for(int r = 0; block_size > r; ++r) {
            for(int c = 0; block_size > c; ++c) {
                board[row][col].addConstraint(new SudokuNotEqualConstraint(
                                          board[row][col],
                                          board[blockRow + r][blockCol + c]));
            }
        }
    }

    public boolean isValidBoard(boolean checkEmpty) {
        // Loosely based upon isValidBoard function in Homework #5.

        for(Variable[] row : board) {
            for(Variable var : row) {
                Constraint current;
                Iterator<Constraint> iterator = var.constraints();

                if((!var.assigned() || var.domain.size() <= 0) && checkEmpty) {
                    return false;
                }

                while(iterator.hasNext()) {
                    current = iterator.next();

                    if(current instanceof SudokuNotEqualConstraint) {
                        if(!((SudokuNotEqualConstraint) current).satisfied()) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    @Override
    public void nodeConsistency() {
        // Maintains node consistency and also makes sure to fill in any
        // null spots on the board before solving.

        for(int r = 0; size > r; ++r) {
            for(int c = 0; size > c; ++c) {
                Variable current = new Variable(r, c);


                if(board[r][c] == null) {
                    board[r][c] = current;
                }
            }
        }

        for(int r = 0; size > r; ++r) {
            for(int c = 0; size > c; ++c) {
                generateConstraints(r, c);
            }
        }
    }

    public Variable minimumRemainingValueNode() {
        Variable node = null;
        int nodeHeuristicValue = Integer.MAX_VALUE;

        for(int r = 0; size > r; ++r) {
            for(int c = 0; size > c; ++c) {
                if(nodeHeuristicValue > board[r][c].domain.size() &&
                   !board[r][c].assigned()) {
                    node = board[r][c];
                    nodeHeuristicValue = node.domain.size();
                }
            }
        }

        return node;
    }

    public void print() {
        // Adapted from Homework 5's board print function

        for (int row = 0; row < size; row++) {
            if (row > 0 && row % block_size == 0) {
                for (int col = 0; col < size; col++) {
                    if (col > 0 && col % block_size == 0) System.out.print("+-");
                    System.out.print("---");
                }
                System.out.println();
            }

            for (int col = 0; col < size; col++) {
                Constraint current;
                Iterator<Constraint> constraints = this.board[row][col].constraints();
                Integer value = null;

                while(constraints.hasNext()) {
                    current = constraints.next();

                    if(current instanceof SudokuEqualConstraint) {
                        value = ((SudokuEqualConstraint) current).value;
                        break;
                    }
                }

                if (col > 0 && col % block_size == 0) System.out.print ("| ");
                if (value != null) {
                    System.out.printf("%2d ", value);
                } else {
                    System.out.print(" . ");
                }
            }
            System.out.println();
        }
    }

    public static SudokuSatisfactionProblem solve(SudokuSatisfactionProblem problem, int depth) {
        Integer[] actions;
        Variable currentVar = problem.minimumRemainingValueNode();

        if(problem.isValidBoard(true)) {
            return problem;
        } else if(currentVar == null) {
            return null;
        } else if(currentVar.domain.size() <= 0) {
            return null;
        }

        actions = currentVar.domain.toArray(new Integer[0]);

        for(int action : actions) {
            assert currentVar.domain.contains(action);

            problem.board[currentVar.row][currentVar.col].addConstraint(
                    problem.new SudokuEqualConstraint(problem.board[currentVar.row]
                                                                   [currentVar.col],
                                                      action));

            // This for block and the latter one after the recursive call
            // helps to maintains node consistency for the temporary board states
            // created in this solve method.
            for(int c = 0; currentVar.constraints.size() > c; ++c) {
                Constraint constraint = currentVar.constraints.get(c);

                if(constraint instanceof SudokuNotEqualConstraint) {
                    if(((SudokuNotEqualConstraint) constraint).left == currentVar &&
                        ((SudokuNotEqualConstraint) constraint).right != currentVar) {
                        ((SudokuNotEqualConstraint) constraint).right.removeFromDomain(action);
                    } else if(((SudokuNotEqualConstraint) constraint).right == currentVar &&
                              ((SudokuNotEqualConstraint) constraint).left != currentVar) {
                        ((SudokuNotEqualConstraint) constraint).left.removeFromDomain(action);
                    }
                }
            }

            SudokuSatisfactionProblem returnValue = solve(problem, depth + 1);

            if(returnValue != null) {
                return problem;
            }

            for(int c = 0; currentVar.constraints.size() > c; ++c) {
                Constraint constraint = currentVar.constraints.get(c);

                if(constraint instanceof SudokuNotEqualConstraint) {
                    if(((SudokuNotEqualConstraint) constraint).left == currentVar &&
                            ((SudokuNotEqualConstraint) constraint).right != currentVar) {
                        ((SudokuNotEqualConstraint) constraint).right.addToDomain(action);
                    } else if(((SudokuNotEqualConstraint) constraint).right == currentVar &&
                            ((SudokuNotEqualConstraint) constraint).left != currentVar) {
                        ((SudokuNotEqualConstraint) constraint).left.addToDomain(action);
                    }
                }
            }

            currentVar.removeLastConstraint();
        }

        return null;
    }

    public static SudokuSatisfactionProblem solve(SudokuSatisfactionProblem problem) {
        return SudokuSatisfactionProblem.solve(problem, 0);
    }
}
