// Bobby Youstra
// Sudoku

public class Main {

    public static void main(String[] args) {
        /*String s = ". 2 4|9 . .|7 3 8 " +
                "8 . 5|. . .|1 6 . " +
                ". 1 9|. . 6|. 5 . " +
                "-----+-----+----- " +
                ". . .|. 1 7|. . 5 " +
                "4 5 .|6 . .|8 . 7 " +
                "9 6 .|2 . .|. 1 . " +
                "-----+-----+----- " +
                ". . .|3 2 .|9 4 . " +
                "1 . 8|. . .|3 . 2 " +
                "2 . 3|. 7 4|. . . ";*/
        /*String s = ". . . | 9 . 6 | . . . " +
                   ". 7 . | . 3 . | . . . " +
                   "6 . . | 4 1 . | . . . " +
                   "------+-------+------ " +
                   ". . . | . . . | . . 8 " +
                   "7 . 2 | . . . | 4 . 9 " +
                   ". 9 . | . . 5 | 6 2 . " +
                   "------+-------+------ " +
                   ". 6 . | . . . | 1 . . " +
                   "5 . . | 8 . 1 | 3 . 4 " +
                   ". . 7 | . . . | . 5 . ";*/
        /*String s = ". . 1|. 6 .|. 3 4 " +
                   ". . 5|4 . .|8 7 1 " +
                   ". . .|. . .|. . . " +
                   "-----+-----+----- " +
                   ". . .|5 . .|. . 3 " +
                   ". 7 .|. . 2|. 1 5 " +
                   "3 . .|. 7 .|. 2 . " +
                   "-----+-----+----- " +
                   ". . 9|. . 4|5 . 8 " +
                   ". . 2|. 9 .|. . . " +
                   ". . .|. . .|. . . ";*/
        /*String s = ". . .|. 7 .|. . . " +
                   ". 8 7|. . .|5 1 4 " +
                   ". . 4|. . .|2 . . " +
                   "-----+-----+----- " +
                   "3 5 .|. . .|. . 2 " +
                   ". . .|. . .|4 . 5 " +
                   ". . 6|1 . .|. 9 . " +
                   "-----+-----+----- " +
                   ". . 5|. . 2|. . . " +
                   ". . .|. 6 1|. . . " +
                   ". 3 .|8 . .|9 . 1 ";*/
        /*String s = ". . 8|. . 7|2 . . " +
                   ". 3 .|. 6 8|7 9 . " +
                   "5 9 .|. 3 .|. . 1 " +
                   "-----+-----+----- " +
                   "1 2 .|. . .|. . . " +
                   ". 8 4|. . .|3 2 . " +
                   ". . .|. . .|. 1 5 " +
                   "-----+-----+----- " +
                   "8 . .|. 4 .|. 3 9 " +
                   ". 1 2|3 9 .|. 4 . " +
                   ". . 9|1 . .|6 . . ";*/
        String s = ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   "-------+-------+-------+------- " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   "-------+-------+-------+------- " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   "-------+-------+-------+------- " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . " +
                   ". . . .|. . . .|. . . .|. . . . ";
        //String s = "";

        if(s.equals("")) {
            for (String arg : args) {
                s += arg;
            }
        }

        SudokuSatisfactionProblem sudokuSatisfactionProblem = new
                                        SudokuSatisfactionProblem(s);

        sudokuSatisfactionProblem.print();
        sudokuSatisfactionProblem.nodeConsistency();
        System.out.println("\n");
        sudokuSatisfactionProblem.print();
        System.out.println("\n");

        //SudokuSatisfactionProblem.solve(sudokuSatisfactionProblem, 0, 0);
        SudokuSatisfactionProblem solution = SudokuSatisfactionProblem.solve(sudokuSatisfactionProblem);

        if(solution == null) {
            System.out.println("\noriginal:");
            sudokuSatisfactionProblem.print();
            System.out.println("Something went wrong...");
        } else {
            sudokuSatisfactionProblem.print();
        }
    }
}
