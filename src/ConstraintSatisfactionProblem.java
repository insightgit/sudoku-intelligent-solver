// Bobby Youstra
// Sudoku
// Example CSP formulation class provided by Mr.Paige
// Modified to adapt it to sudoku and for it to compile

import java.util.HashSet;
import java.util.Iterator;
import java.lang.Iterable;

public abstract class ConstraintSatisfactionProblem {

	public abstract class Variable<T> {
		
		public abstract String name();
		public abstract void addToDomain(T value);
		public abstract void removeFromDomain(T value);
		public abstract boolean isInDomain(T value);
		public abstract HashSet<T> domain();

		public abstract Iterator<Constraint> constraints();

		protected abstract boolean assigned();
	}

	public abstract class Constraint {
	}


	public abstract class UnaryConstraint<T> extends Constraint {

		private Variable<T> variable;

		public UnaryConstraint(Variable<T> variable) {
			this.variable = variable;
		}

		public Variable<T> variable() {
			return this.variable;
		}

		public abstract boolean satisfiedBy(T value);

		public boolean satisfied() {
			for (T value : variable.domain()) {
				if (!satisfiedBy(value)) return false;
			}
			return true;
		}
	}

	public abstract class EqualConstraint<T> extends UnaryConstraint<T> {

		protected T value;

		public EqualConstraint(Variable variable, T value) {
			super(variable);
			this.value = value;
		}

		public boolean satisfiedBy(T value) {
			return this.value.equals(value);
		}
	}
		

	public abstract class BinaryConstraint<T1, T2> extends Constraint {

		protected Variable<T1> left;
		protected Variable<T2> right;

		public Variable<T1> leftVariable() { return this.left; }
		public Variable<T2> rightVariable() { return this.right; }

		public abstract boolean satisfiedBy(T1 leftValue, T2 rightValue);
		/*public abstract boolean canBeSatisfiedByLeftValue(T1 leftValue);
		public abstract boolean canBeSatisfiedByRightValue(T2 rightValue);*/

		public boolean satisfied() {
			if(left == right) {
				return true;
			}

			for (T1 leftValue : left.domain()) {
				for (T2 rightValue : right.domain()) {
					if (!satisfiedBy(leftValue, rightValue)) return false;
				}
			}
			return true;
		}
	}

	public abstract class NotEqualConstraint<T> extends BinaryConstraint<T, T> {

		public NotEqualConstraint(Variable<T> left, Variable<T> right) {
			this.left = left;
			this.right = right;
		}

		public boolean satisfiedBy(T leftValue, T rightValue) {
			return !leftValue.equals(rightValue);
		}
	}

	public abstract void nodeConsistency();

	/*public arcConsistency() {  // AC-3
	}

	public pathConsistency() { // PC-2
	}*/

}
